package ua.epam.repository;

import ua.epam.model.AccountStatus;

public interface AccountStatusRepository extends GenericRepository<AccountStatus, Long> {
}
