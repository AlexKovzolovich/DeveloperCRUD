package ua.epam.service;

import ua.epam.model.Skill;

public interface SkillService extends Service<Skill, Long> {
}
