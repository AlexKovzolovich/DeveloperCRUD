package ua.epam.service;

import ua.epam.model.Developer;

public interface DeveloperService extends Service<Developer, Long> {
}
