package ua.epam.service;

import ua.epam.model.Account;

public interface AccountService extends Service<Account, Long> {
}
